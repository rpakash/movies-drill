const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

function moviesWithEarnings(movies, earnings) {
  return Object.entries(movies)
    .filter(([name, details]) => {
      let number = details.totalEarnings.replace("$", "").replace("M", "");

      return Number(number) > earnings;
    })
    .reduce((accu, [name, details]) => {
      accu[name] = details;

      return accu;
    }, {});
}

function movieWithOscarAndEarnings(movies, oscar, earnings) {
  return Object.entries(movies)
    .filter(([name, details]) => {
      let number = details.totalEarnings.replace("$", "").replace("M", "");

      return Number(number) > earnings && details.oscarNominations > oscar;
    })
    .reduce((accu, [name, details]) => {
      accu[name] = details;

      return accu;
    }, {});
}

function moviesWithActor(movies, actorName) {
  return Object.entries(movies)
    .filter(([name, details]) => {
      return details.actors.includes(actorName);
    })
    .reduce((accu, [name, details]) => {
      accu[name] = details;

      return accu;
    }, {});
}

function sortMovies(movies) {
  return Object.entries(movies)
    .sort(([movieA, movieADetails], [movieB, movieBDetails]) => {
      let subtractedRating = movieBDetails.imdbRating - movieADetails.imdbRating;

      if (subtractedRating === 0) {
        let movieAEarning = movieADetails.totalEarnings.replace("$", "").replace("M", "");
        let movieBEarning = movieBDetails.totalEarnings.replace("$", "").replace("M", "");

        return movieBEarning - movieAEarning;
      } else {
        return subtractedRating;
      }
    })
    .reduce((accu, [name, details]) => {
      accu[name] = details;

      return accu;
    }, {});
}

function groupMovies(movies) {
  return Object.entries(movies).reduce((accu, [name, details]) => {
    if (details.genre.length === 1) {
      if (accu[details.genre[0]] === undefined) {
        accu[details.genre[0]] = [{ [name]: details }];
      } else {
        accu[details.genre[0]].push({ [name]: details });
      }
    } else {
      let genreArray = ["drama", "sci-fi", "adventure", "thriller", "crime"];
      let genrePriority = { ...genreArray };

      let genre = details.genre
        .map((type) => {
          return genrePriority[type];
        })
        .sort((numberA, numberB) => {
          return numberA - numberB;
        })[0];

      genre = genreArray[genre];

      if (accu[genre] === undefined) {
        accu[genre] = [{ [name]: details }];
      } else {
        accu[genre].push({ [name]: details });
      }
    }

    return accu;
  }, {});
}
